//
//  ViewModel.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

// ViewModel

import SwiftUI

class viewModel2048: ObservableObject {
    // global but only within this class
    // Connect to model
    // In classes all vars need to have a value or init
    // private means that only this class can see and edit the variable
    // private(set) allows for external viewing but no edits to the variable
    static func createmodel2048() -> model2048<Int> {
        model2048<Int>(tilesPerSection: 4)
    }
    
    @Published private var model: model2048<Int> = viewModel2048.createmodel2048()
    
    var tiles: Array<model2048<Int>.Tile>{
        var tileList: Array<model2048<Int>.Tile> = []
        model.tiles.forEach{ tileRow in
            tileRow.forEach{
               tile in tileList.append(tile)
            }
        }
        return tileList
    }
    
    var gameStat: String = "KEEP GOING!"
    
    var score: Int {
        return model.score
    }
    
    var best: Int = 0
    
    // isGameOver
    var isGameOver: Bool {
        let gameStatus: Int = getGameStatus
        print(String(gameStatus))
        if gameStatus == 0{
            return false
        }
        return true
    }
    
    var getGameStatus: Int{
        return model.getGameStatus()
    }
    
    func updateBest(){
        if score > best{
            best = score
        }
    }
    
    // MARK: - Intent(s)
    
    //temporary mimc of swipe gesture
    func swipe(_ direction: String) -> Void{
        print(direction)
        
        // If game is over, don't let user proceed
        if isGameOver {gameStat = "YOU LOST!"
            updateBest()
            return}
        else{
        // Else, do swipe gestures
        switch direction{
            case "L": model.swipeLeft()
                
                print("left")
            case "U": model.swipeUp()
                print("up")
            case "D": model.swipeDown()
                print("down")
            case "R": model.swipeRight()
                print("right")
            default: print("no swipe")
        }
        if(model.checkWon()) {
            updateBest()
            gameStat = "YOU WON!"
            return}
            // Generate tile after swipe gesture
            if !model.isFilled(){
                model.generateTile()
            }
            if isGameOver {
                updateBest()
                gameStat = "YOU LOST!"
                return}
        }
    }
    
    // Func to restart game
    func restart() -> Void{
        gameStat = "KEEP GOING!"
        model.restart()
    }
}
