//
//  BoardView.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

// BoardView
import SwiftUI

//"View" is a protocol. It indicates how this struct should behave
// A view basically the screen
// View requires a body
struct BoardView: View {
    let tiles: Array<model2048<Int>.Tile>
    //some view = something that behaves like a view
    //some view assumes the return type of body
    //body is a combiner view that encapsulates various smaller views
    //body is technically a function (without any parameters)
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10).fill(Color("gridBg"))
            VStack {
                LazyVGrid(columns: [GridItem(),GridItem(),GridItem(),GridItem()]){
                    ForEach(tiles) { tile in TileView(tile: tile).aspectRatio(1/1, contentMode: .fit)}
                }.padding()
            }
        }
    }
}

// Structs are immutable meaning they can't be changed
struct TileView: View{
    let tile: model2048<Int>.Tile
    var body: some View{
        // Zstacks can't have for loops
        if (tile.gameStatus == "Playing") {
            ZStack {
                let shape = RoundedRectangle(cornerRadius: 5)
                shape.fill(Color(String(tile.content)))
                let textFormat = Text(String(tile.content)).fontWeight(.bold)
                switch tile.content{
                    case 2, 4:
                        textFormat.font(.largeTitle).foregroundColor(Color("DarkBrown"))
                    case 8, 16, 32, 64, 128, 256, 512:
                        textFormat.font(.title).foregroundColor(.white)
                    case 1024, 2048:
                        textFormat.font(.title2).foregroundColor(.white)
                    default:
                        shape.fill(Color("0"))
                }
            }
        }
        else if(tile.gameStatus == "You Won") {
            ZStack {
                RoundedRectangle(cornerRadius: 5).fill(Color.yellow)
                Text("2048").fontWeight(.bold).font(.title2).foregroundColor(.white)
            }
        }
        else {
            ZStack {
                RoundedRectangle(cornerRadius: 5).fill(Color("64"))
                Text(tile.gameStatus).fontWeight(.bold).font(.title2).foregroundColor(.white)
            }
        }
    }
}
