//
//  MenuView.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

import SwiftUI

struct MenuView: View {
    let game = viewModel2048()
    @Environment(\.presentationMode) var presentationMode
    @State var showInstructions: Bool = false
    var body: some View {
        NavigationView {
            ZStack {
                Color("backgroundColor").ignoresSafeArea()
                GeometryReader {geo in
                    VStack {
                        Text("Menu").font(.largeTitle).fontWeight(.bold).foregroundColor(Color("DarkBrown"))
                        Button {
                            presentationMode.wrappedValue.dismiss()
                        } label: {Text("KEEP GOING").bold().font(.title2)}.buttonStyle(MenuBtnStyle())
                        Button {
                            /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                        } label: {
                            Text("CHALLENGES").bold().font(.title2)
                        }.buttonStyle(MenuBtnStyle())
                    
                        Button {
                            /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                        } label: {
                            Text("MULTIPLAYER").bold().font(.title2)
                        }.buttonStyle(MenuBtnStyle())
                        Button {
                            showInstructions.toggle()
                        } label: {
                            Text("HOW TO PLAY").bold().font(.title2)}.buttonStyle(MenuBtnStyle()).sheet(isPresented: $showInstructions, content: { InstructionView()})
                        Button {
                            /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                        } label: {
                            Text("RATE").bold().font(.title)
                        }.buttonStyle(MenuBtnStyle())
                    }.scaledToFit().frame(width: geo.size.width * 0.8).frame(width: geo.size.width, height: geo.size.height)
                }
            }
        }.navigationViewStyle(.stack)
    }
}

struct MenuBtnStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label.cornerRadius(10).padding().frame(width: 200.0, height: 50.0).background(Color("LightBrown")).foregroundColor(.white)
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
