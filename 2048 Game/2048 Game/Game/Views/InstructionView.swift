//
//  InstructionView.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

import SwiftUI

struct InstructionView: View {
    @Environment(\.presentationMode) var presentationMode
    let instructions: [String] = ["1. Click on the direction buttons to swipe all tiles", "2. 2 tiles with the same number merge into one when combined!", "3. A tile will be generated with each swipe", "4. It's over when the board fills up...", "5. Join the numbers and get to the 2048 tile!"]
    var body: some View {
        ZStack{
            Color("16").ignoresSafeArea()
            GeometryReader{geo in
                VStack {
                    Text("HOW TO PLAY").font(.largeTitle).fontWeight(.bold).padding(.top, 70).foregroundColor(Color("white"))
                    List{
                        Section(){
                            ForEach(instructions, id: \.self){
                                instruction in Text(instruction).font(.body).fontWeight(.medium).foregroundColor(Color(.black))
                            }
                        
                        }
                    }
                }.frame(width: geo.size.width * 0.8).frame(width: geo.size.width, height: geo.size.height)
            }
        }
    }
}

struct InstructionView_Previews: PreviewProvider {
    static var previews: some View {
        InstructionView()
    }
}
