//
//  EndView.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

import SwiftUI

struct EndView: View {
    let game = viewModel2048()
    @Environment(\.presentationMode) var presentationMode
    var gameStat: String
    let score: Int
    
    var body: some View {
        NavigationView{
            GeometryReader {geo in
                ZStack {
                    Color("backgroundColor").ignoresSafeArea()
                    VStack {
                        Text(gameStat).font(.largeTitle).fontWeight(.bold).foregroundColor(Color("DarkBrown"))
                        Button {} label: {
                            VStack{
                                Text("SCORE").font(.title2).fontWeight(.bold).foregroundColor(Color("backgroundColor"))
                                Text(String(score)).font(.title).fontWeight(.bold).foregroundColor(.white)
                            }
                        }.padding().frame(width: 200.0, height: 80.0).background(Color("LightBrown")).foregroundColor(Color.white)
                        Button {
                            presentationMode.wrappedValue.dismiss()
                        } label: {
                            Text("BACK TO BOARD").bold().font(.title3)
                        }.padding().frame(width: 200.0, height: 50.0).background(Color("LightBrown")).foregroundColor(Color.white)
                        Button(action: actionSheet) {
                            HStack {
                                Image(systemName: "square.and.arrow.up").resizable().aspectRatio(contentMode: .fit).frame(width: geo.size.width * 0.07)
                                Text("SHARE").bold().font(.title).padding(5).foregroundColor(Color.white)
                            }
                        }.frame(width: 200.0, height: 50.0).background(Color("LightBrown"))
                    }.scaledToFit().frame(width: geo.size.width * 0.6).frame(width: geo.size.width, height: geo.size.height)
                }
            }
        }.navigationViewStyle(.stack)
    }
    func actionSheet() {
        guard let urlShare = URL(string: "https://developer.apple.com/xcode/swiftui/") else { return }
        let activityVC = UIActivityViewController(activityItems: [urlShare], applicationActivities: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(activityVC, animated: true, completion: nil)
    }
}
