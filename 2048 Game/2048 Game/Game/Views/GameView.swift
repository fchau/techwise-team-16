//
//  GameView.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

import SwiftUI

struct CustomColor {
    static let myColor = Color("backgroundColor")
}

struct GameView: View {
    // Declaring Variables needed for view
    @ObservedObject var viewModel: viewModel2048
    @State var showMenu: Bool = false
    @State var showEnd: Bool = false
    @State var offset: CGSize = .zero
    @State var currScore: Int = 0
    
    // View
    var body: some View {
        ZStack {
            Color("backgroundColor").ignoresSafeArea()
            GeometryReader { geo in
                VStack {
                    HStack {
                        ZStack{
                            RoundedRectangle(cornerRadius: 10).fill(Color.yellow)
                            Text("2048").font(.largeTitle).fontWeight(.bold).foregroundColor(.white)
                        }.frame(width: geo.size.width * 0.33, height: geo.size.height * 0.175)
                        VStack {
                            HStack {
                                let brownShape = RoundedRectangle(cornerRadius: 5).fill(Color("LightBrown"))
                                ZStack {
                                    brownShape
                                    VStack {
                                        Text("SCORE").font(.title2).fontWeight(.bold).foregroundColor(Color("backgroundColor"))
                                        Text(String(viewModel.score)).font(.title).fontWeight(.bold).foregroundColor(.white)
                                    }
                                }
                                ZStack {
                                    brownShape
                                    VStack {
                                        Text("BEST").font(.title2).fontWeight(.bold).foregroundColor(Color("backgroundColor"))
                                        
                                        Text(String(viewModel.best)).font(.title).fontWeight(.bold).foregroundColor(.white)
                                    }
                                }
                            }.frame(width: geo.size.width * 0.55, height: geo.size.height * 0.108)
                            HStack {
                                let orangeShape = RoundedRectangle(cornerRadius: 5).fill(Color.orange)
                                ZStack {
                                    orangeShape
                                    Button("MENU"){
                                        showMenu.toggle()
                                    }.buttonStyle(OrangeBtnStyle()).fullScreenCover(isPresented: $showMenu, content: { MenuView()})
                                    
                                }
                                ZStack {
                                    orangeShape
                                    Button("NEW GAME"){
                                        viewModel.restart()
                                    }.buttonStyle(OrangeBtnStyle())
                                }
                            }.frame(width: geo.size.width * 0.55, height: geo.size.height * 0.035)
                        }
                    }
                    Text("Join the numbers and get to the 2048 tile!").font(.headline).fontWeight(.bold).multilineTextAlignment(.leading).foregroundColor(Color("DarkBrown")).frame(height: geo.size.height * 0.1)
                    let directions: [swipes] = [swipes(direction: "LEFT", letter: "L", id: 1), swipes(direction: "UP", letter: "U", id: 2), swipes(direction: "DOWN", letter: "D", id: 3), swipes(direction: "RIGHT", letter: "R", id: 4)]
                    HStack {
                        ForEach(directions){item in
                            Button(item.direction){
                                viewModel.swipe(item.letter)
                            }.buttonStyle(OrangeBtnStyle()).scaledToFit().frame(width: geo.size.width * 0.2, height: geo.size.height * 0.05).background(Color("Blue"))
                        }
                    }
                    BoardView(tiles: viewModel.tiles).padding(.top)
                    
                    Button {
                        currScore = viewModel.score
                        showEnd.toggle()
                        if viewModel.gameStat != "KEEP GOING!"{
                            viewModel.restart()
                        }
                    }label: {
                        Text(String(viewModel.gameStat)).font(.title).fontWeight(.bold).foregroundColor(.blue)}.fullScreenCover(isPresented: $showEnd, content: {
                                EndView(gameStat: viewModel.gameStat, score: currScore)}).frame(height: geo.size.height * 0.07)
                    
                }.scaledToFit().frame(width: geo.size.width * 0.9).frame(width: geo.size.width, height: geo.size.height)
            }
        }
    }
}

// Orange Button Style
struct OrangeBtnStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label.font(.subheadline).foregroundColor(.white)
    }
}

// Struct to manage swipe
struct swipes: Identifiable {
    var direction: String
    var letter: String
    var id: Int
}

// For generating a preview
struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        let game = viewModel2048()
        GameView(viewModel: game).previewInterfaceOrientation(.portrait)
    }
}
