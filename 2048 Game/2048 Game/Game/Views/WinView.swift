//
//  WinView.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

import SwiftUI

struct WinView: View {
    var body: some View {
        ZStack{
            Color("2048").ignoresSafeArea()
            VStack{
                Text("You win!").font(.largeTitle).fontWeight(.bold).foregroundColor(.white).padding().frame(width: 300.0, height: 200.0)
                HStack{
                    Button {
                        /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                    } label: {
                        Text("Keep Going").bold().font(.title2)
                    }.buttonStyle(WinBtnStyle())
                    Button {
                        /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                    } label: {
                        Text("Try Again").bold().font(.title2)
                    }.buttonStyle(WinBtnStyle())
                }
            }
        }
    }
}

struct WinBtnStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label.padding().frame(width: 200.0, height: 50.0).background(Color("LightBrown")).foregroundColor(Color.white)
    }
}

struct WinView_Previews: PreviewProvider {
    static var previews: some View {
        WinView()
    }
}
