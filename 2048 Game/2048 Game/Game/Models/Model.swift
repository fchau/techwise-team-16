//
//  Model.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

// Model
// Foundation package holds basic structs such as array
import Foundation

// Tile Content is a generic (basically placeholder for type)
struct model2048<TileContent> {
   
    // Declaring a var for scoring score
    private(set) var score = 0
    
    // Declaring a 2d array pre-populated with type Tile
    private(set) var tiles: [[Tile]]
    
    // Function to check if program can continue matching row or columns
    mutating func canContinueMatchingArr() -> Bool {
        if rowHasRemainingMatches() || colHasRemainingMatches() {
            return true
        }
        
        return false
    }
    
    // Function to check if user has won
    mutating func checkWon() -> Bool {
        for row in tiles{
            for tile in row{
                if tile.content == 2048 {
                    youWon()
                    print("You won!")
                    return true
                }
            }
        }
        return false
    }
    
    // Not mutating func to check if tiles in column can be matched
    func colHasRemainingMatches() -> Bool {
        var rotatedArr: [[Tile]] = tiles
        
        for row in 0..<rotatedArr.count{
            for col in 0..<rotatedArr.count{
                rotatedArr[row][col] = tiles[col][row]
            }
        }
        
        for i in 0..<rotatedArr.count{
            if hasRemainingMatches(tempArr: rotatedArr, rIndex: i){
                return true
            }
        }
        return false
    }
    
    // Function to handle swipeLogi
    // Parameter indicates index of the row
    mutating func combineRow(rIndex: Int) -> Void {
        // Intializing an empty array of Integers
        var row: [Int] = []
        
        // Copying row of 2d array tiles into temporary array
        for tile in tiles[rIndex]{
            row.append(tile.content)
        }
        
        // Remove zeroes from row
        row = removeZeroesFromArr(row: row)
        
        // Combine tiles
        row = combineTiles(row: row)
        
        // Get number of newTiles needed to construct row
        let newTiles: Int = 4 - row.count
        
        // Construct row with 4 elements
        for _ in 0..<newTiles{
            row.append(0)
        }
        
        // Add combined row to tiles 2d array
        for j in 0..<row.count{
            tiles[rIndex][j].content = row[j]
        }
        
    }
    
    // Function to combine tiles in a row
    mutating func combineTiles(row: [Int]) -> [Int] {
        var temp = row
        
        var i: Int = 0
        
        // Adds Matching Tiles
        while i < (temp.count-1){
            if temp[i] == temp[i+1]{
                temp[i] *= 2
                
                // Updating score based on added tile value
                updateScore(amount: temp[i])
                
                temp.remove(at: i+1)
            }
            i+=1
        }
        return temp
    }
    
    // Function to get index of emptyTiles
    mutating func emptyTiles() -> [Int] {
        // Declare an empty array of Integers
        var emptyPos = [Int]()
        
        // Iterate through tiles to find where the tile is equal to 0
        for row in tiles{
            for tile in row{
                if tile.content == 0 {
                    // Append id to array
                    emptyPos.append(tile.id)
                }
            }
        }
        
        // Return an array of empty ids
        return emptyPos
    }
    
    mutating func generateTile() -> Void {
        // Get indices of empty positions in the tiles array
        let emptyPos = emptyTiles()
        
        // Get random tile index from array
        let randomPos = emptyPos.randomElement()!
        
        // Randomly choose either 2 or 4 for tile value
        let twoorfour = [2, 4]
        let twoOrFour = twoorfour.randomElement()!
        
        // Iterate through tiles array, find matching id and insert 2 or 4
        for row in 0..<tiles.count{
            for col in 0..<tiles.count{
                if tiles[row][col].id == Int(randomPos) {
                    tiles[row][col].content = twoOrFour
                }
            }
        }
    }
    
    // Function to check if game is over
    mutating func getGameStatus() -> Int {
        for row in tiles{
            for tile in row{
                if tile.content == 0 {
                    print("Keep going")
                    return 0
                }
                else if canContinueMatchingArr() {
                    print("Keep going")
                    return 0
                }
            }
        }
        youLost()
        print("You lost")
        return -1
    }
    
    // Func to check for remaining matches
    func hasRemainingMatches(tempArr: [[Tile]],rIndex: Int) -> Bool {
        var temp: [Int] = []
        
        for card in tempArr[rIndex]{
            temp.append(card.content)
        }
        
        var i: Int = 0
        
        //omits zeros
        while i < temp.count{
            if temp[i] == 0{
                temp.remove(at: i)
            }else{
                i += 1
            }
        }
        
        i = 0
        
        // checks for matching tiles
        while i < (temp.count-1){
            if temp[i] == temp[i+1]{
                return true
            }
            i+=1
        }
        
        return false
    }
    
    // Function to check if the tiles 2d array is filled
    func isFilled() -> Bool{
        for row in tiles{
            for tile in row{
                if tile.content == 0{
                    return false
                }
            }
        }
        return true
    }
    
    // Function to remove zeroes from an Array
    mutating func removeZeroesFromArr(row: [Int]) -> [Int] {
        // Copy content of row to allow modification
        var temp = row
        
        // Declaring a variable
        var i: Int = 0
        
        // Omit zeroes from temporary array
        while i < temp.count{
            if temp[i] == 0{
                temp.remove(at: i)
            }else{
                i += 1
            }
        }
        
        // Return row with removed zero values
        return temp
    }
    
    // Function to reset board
    mutating func restart() -> Void {
        score = 0
        for row in 0..<tiles.count{
            for col in 0..<tiles.count{
                tiles[row][col].content = 0
                tiles[row][col].gameStatus = "Playing"
            }
        }
        generateTile()
    }

    // Function to reverse 2d array
    mutating func reverse2DArr() -> Void {
        for i in 0..<tiles.count{
            tiles[i].reverse()
        }
    }
    
    // Function to handle rotating array by 90 degrees
    mutating func rotateArray90Deg() -> Void {
        // Declaring a temporary 2d array
        let temp: [[Tile]] = tiles
        
        for row in 0..<tiles.count{
            for col in 0..<tiles.count{
                tiles[row][col] = temp[col][row]
            }
        }
    }
    
    // Non-mutating func that check if row has any remaining matches
    func rowHasRemainingMatches() -> Bool {
        let tempArr: [[Tile]] = tiles
        for i in 0..<tiles.count{
            if hasRemainingMatches(tempArr: tempArr, rIndex: i){
                return true
            }
        }
        return false
    }
    
    // Function to handle swipeDown logic
    mutating func swipeDown() -> Void {
        rotateArray90Deg()
        swipeRight()
        rotateArray90Deg()
    }
    
    // Function to handle swipeLeft logic
    mutating func swipeLeft() -> Void {
        for i in 0..<tiles.count{
            combineRow(rIndex: i)
        }
    }
    
    // Function to handle swipeRight logic
    mutating func swipeRight() -> Void {
        reverse2DArr()
        swipeLeft()
        reverse2DArr()
        
    }
    
    // Function to handle swipeUp Logic
    mutating func swipeUp() -> Void {
        rotateArray90Deg()
        swipeLeft()
        rotateArray90Deg()
    }
    
    // Function to update score
    mutating func updateScore(amount: Int) -> Void{
        score += amount
    }
    
    // Function to handle game status update after losing
    mutating func youLost() -> Void {
        for row in 0..<tiles.count{
            for col in 0..<tiles.count{
                tiles[row][col].gameStatus = "You Lost"
            }
        }
    }
    
    // Function to handle game status update after winning
    mutating func youWon() -> Void {
        for row in 0..<tiles.count{
            for col in 0..<tiles.count{
                tiles[row][col].gameStatus = "You Won"
            }
        }
    }
    
    // Struct that defines tile
    struct Tile: Identifiable {
        var content: Int = 0
        var id: Int
        var gameStatus: String = "Playing"
    }
    
    // Initalizer: adds Tile to array of tiles, initializes other variables
    init(tilesPerSection: Int){
        tiles = [[Tile]] ()
        var tileID: Int = 0
        for _ in 0..<tilesPerSection{
            var row: [Tile] = []
            for _ in 0..<tilesPerSection{
                row.append(Tile(id: tileID))
                tileID += 1
            }
            tiles.append(row)
        }
        
        //Insert Initial Tile to start game
        if !isFilled() {
            print("Generating First Tile")
            generateTile()
        }
    }
}
