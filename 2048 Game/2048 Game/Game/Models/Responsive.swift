//
//  Responsive.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

import UIKit

enum Responsive {
    //Gets screen width
    static var screenWidth: CGFloat{
        UIScreen.main.bounds.size.width
    }
    
    //Gets screen height
    static var screenHeight: CGFloat {
        UIScreen.main.bounds.size.height
    }
    
    //determines if screen is portrait or landscape
    static var minDimension: CGFloat {
        min(screenWidth, screenHeight)
    }
    
    //determines boardwidth based on mindimension
    static var boardWidth: CGFloat {
        switch minDimension{
        case 0...320:
            return screenWidth - 55
        case 321...430:
            return screenWidth - 50
        case 431...1000:
            return 350
        default:
            return 500
        }
    }
}
