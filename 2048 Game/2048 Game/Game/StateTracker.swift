//
//  State.swift
//  2048 Game
//
//  Created by dmcadmin on 8/2/22.
//

import Foundation

typealias GameState = (board: [[Int]], score: Int)

protocol StateTracker {
    func next(with state: GameState) -> GameState
    func updateCurrent(with board: [[Int]]) -> GameState
    func reset(with state: GameState) -> GameState

    var isUndoable: Bool {get}
    func undo() -> GameState
    
    var statesCount: Int {get}
    var last: GameState {get}
}

