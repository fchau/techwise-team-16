//
//  _048_GameApp.swift
//
//  Team 16 - TechWise
//  Fiorina Chau
//  Nikhil Sharma

import SwiftUI
@main
struct _048_GameApp: App {
    let game = viewModel2048()
    var body: some Scene {
        WindowGroup {
            GameView(viewModel: game)
        }
    }
}

extension View {
    /// Navigate to a new view.
    /// - Parameters:
    ///   - view: View to navigate to.
    ///   - binding: Only navigates when this condition is `true`.
    func navigate<NewView: View>(to view: NewView, when binding: Binding<Bool>) -> some View {
        NavigationView {
            ZStack {
                self
                    .navigationBarTitle("")
                    .navigationBarHidden(true)

                NavigationLink(
                    destination: view
                        .navigationBarTitle("")
                        .navigationBarHidden(true),
                    isActive: binding
                ) {
                    EmptyView()
                }
            }
        }
        .navigationViewStyle(.stack)
    }
}
